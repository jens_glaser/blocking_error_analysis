import sys
import numpy

import BlockAverage

from matplotlib import pyplot

if len(sys.argv) != 2:
    print('Usage: python test_blockingerr.py <input filename>')
    sys.exit(1)

plotting=True
data = numpy.loadtxt(sys.argv[1])
block = BlockAverage.BlockAverage(data)
(n, num, err, err_err) = block.get_hierarchical_errors()

print('Hierarchical error analysis:')
for (i, num_samples, e, ee) in zip(n, num, err, err_err):
    print('{0} {1} {2} {3}'.format(i,num_samples,e,ee))

if plotting:
    pyplot.figure()
    pyplot.errorbar(x=n, y=err, yerr=err_err)
    print('Saving in hierarchical_error.pdf')
    pyplot.savefig('hierarchical_error.pdf')


(i, err_est) = block.get_error_estimate()
print('Mean value: {0}'.format(numpy.mean(data)))
print('Approx. # of uncorrelated samples: {0}'.format(num[i]))
print('Estimated error of the mean: {0}'.format(err_est))
