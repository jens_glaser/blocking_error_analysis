import numpy as np
import math

# length of data
num_samples = 2000

# mean of data
mu = [0.0]*num_samples

# correlation length (standard deviation of Gaussian kerenl)
sigma = 20

# The desired covariance matrix.
r = np.ndarray(shape=(num_samples,num_samples))
for i in range(num_samples):
    for j in range(num_samples):
        r[i,j] = 1.0/math.sqrt(2.0*math.pi*sigma*sigma)*math.exp(-(i-j)*(i-j)/2.0/sigma/sigma)

# Generate the random samples.
y = np.random.multivariate_normal(mu, r, size=1)
for val in y[0]:
    print('{0}'.format(val))

